"use client";
import { Inter } from '@next/font/google';

import "./globals.css"
import React from "react";
import BgRectangles from "../components/BgRectangles.js";
import Map from "../components/Map.js";

import { useUserAgent } from 'next-useragent';

const inter = Inter({
    variable: '--font-inter',
});

export const UA = React.createContext(null)

export default function RootLayout({children}) {

    const [uA, setUa] = React.useState({})

  React.useMemo(() => {
      if (typeof window != "undefined") {
          setUa(useUserAgent(window.navigator.userAgent))
      }
    },[])

    // @ts-ignore
    return (
            <html lang="en" className={`${inter.variable} font-sans`}>
                <head />
                <body>
                    <UA.Provider value = {uA}>
                    <BgRectangles/>
                    <Map/>
                    {children}
                    </UA.Provider>
                </body>

            </html>
            )
}
