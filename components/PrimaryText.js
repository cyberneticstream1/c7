"use client";
import React from "react";
import {UA} from "../app/layout";
export default function PrimaryText({children}){
    const [ua] = React.useState(React.useContext(UA))
    const [mobileVisibility, setMobileVisibility] = React.useState("hidden")
    const [pCVisibility, setPCVisibility] = React.useState("hidden")

    React.useEffect(() => {
        if (ua.isMobile) {
            setMobileVisibility("block")
            setPCVisibility("hidden")
        } else{
            setMobileVisibility("hidden")
            setPCVisibility("block")
        }
    },[])

    return(
            <>
            <div  className={"text-white text-sans text-5xl leading-tight"}>
            <div className = {mobileVisibility + " fixed bottom-0 w-11/12 h-80 left-1/2 -translate-x-1/2 rounded-xl z-10"}>
                <div className={""}>
                {children}
                </div>
            </div>
            <div className = {pCVisibility + " fixed top-1/2 -translate-y-1/2 right-1/4 translate-x-1/2  w-96 h-96 z-10"}>
                <div className={""}>
                {children}
                </div>
            </div>
           </div>
            </>
    )
}