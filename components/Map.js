"use client";
import Script from "next/script";
import * as React from 'react';
import {UA} from "../app/layout";

async function getRegionData(){
    const data = fetch("https://maps.googleapis.com/maps/api/geocode/json?address=929leeavesanleandroca&key=AIzaSyDpoL4EfSObNpf8tMJv3mHI2b24aU2pooc").then(x => x.json())
    return data
}

export default function Map(){
    const [ua] = React.useState(React.useContext(UA))
    const [regionData, setRegionData] = React.useState(parseGeocodingAPI(React.use(getRegionData())))
    const [dLat, setDLat] = React.useState((ua.isMobile) ? (-0.2) : (-0.09))
    const [dLng, setDLng] = React.useState((ua.isMobile) ? (0 ): (0.15))




    function parseGeocodingAPI(obj){
        return obj["results"][0]
    }

    console.log(regionData)

    const main = async() => {
        await setupMapKitJs();

        const map = new mapkit.Map("map-container");
        map.mapType = mapkit.Map.MapTypes.Satellite
        map.region = new mapkit.CoordinateRegion( new mapkit.Coordinate(regionData.geometry.location.lat + dLat,regionData.geometry.location.lng + dLng), new mapkit.CoordinateSpan(0.65, 0.65));


        const propertyAnnotation = new mapkit.MarkerAnnotation(new mapkit.Coordinate(regionData.geometry.location.lat, regionData.geometry.location.lng));
        propertyAnnotation.color = "#969696";
        propertyAnnotation.selected = "true";
        propertyAnnotation.glyphText = "🛩️";
        map.addItems([ propertyAnnotation]);

    };

    const setupMapKitJs = async() => {
        if (!window.mapkit || window.mapkit.loadedLibraries.length === 0) {
            await new Promise(resolve => { window.initMapKit = resolve });
            delete window.initMapKit;
        }
        const jwt = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IllIWlgzNjlHN0gifQ.eyJpc3MiOiJRUzhTM01LVTZMIiwiaWF0IjoxNjY3OTcwNTU2LCJleHAiOjE2NzA1NjI1MDR9.86HtzzR6G-Cb4mluBQ9YkBrIBlOMCpZA_zNWGR_en_shRinfy8DDyCgGOwHmpXQU_qr1wTDIgwFRqA5NpSub3Q";
        mapkit.init({
            authorizationCallback: done => { done(jwt); }
        });
    };

    return(
            <>
            <Script src="https://cdn.apple-mapkit.com/mk/5.x.x/mapkit.core.js" async data-callback="initMapKit" beforeInteractive={true} data-libraries="map,annotations,services" data-initial-token="" onReady={()=> {main();}}></Script>
            <div id="map-container" className={"map w-full h-screen z-0" }/>
            </>
            )
}