"use client";
import { useUserAgent } from 'next-useragent'
import React from 'react'
import {UA} from "../app/layout";

export default function BgRectangles(){

    const [ua] = React.useState(React.useContext(UA))
    const [mobileVisibility, setMobileVisibility] = React.useState("hidden")
    const [pCVisibility, setPCVisibility] = React.useState("hidden")

    console.log(ua)
    React.useEffect(() => {
        if (ua.isMobile) {
            setMobileVisibility("block")
            setPCVisibility("hidden")
        } else{
            setMobileVisibility("hidden")
            setPCVisibility("block")
        }
    },[])

    return(
            <>
            <div className = {mobileVisibility + " fixed bottom-0 w-11/12 h-80 left-1/2 -translate-x-1/2 bg-slate-300 opacity-50 rounded-xl shadow-sm shadow-slate-300 z-10"}/>
            <div className = {pCVisibility + " fixed top-1/2 -translate-y-1/2 right-1/4 translate-x-1/2  w-96 h-96  bg-slate-300 opacity-50 rounded-xl shadow-sm shadow-slate-300 z-10"}/>
            </>
            )

}
